package com.jumpshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<WordCountPair> resultList = new ArrayList();
        resultList.add(new WordCountPair("word-1", 1));
        resultList.add(new WordCountPair("word-2", 2));
        resultList.add(new WordCountPair("word-3", 3));

        for (int i = 0; i < resultList.size(); i++) {
            System.out.println(resultList.get(i));
        }

        Collections.sort(resultList,Collections.reverseOrder());

        System.out.println("*************************************");
        for (int i = 0; i < resultList.size(); i++) {
            System.out.println(resultList.get(i));
        }
    }
}

class WordCountPair implements Comparable<WordCountPair> {
    public String getWord() {
        return word;
    }

    public int getCount() {
        return count;
    }

    private String word;
    private Integer count;

    WordCountPair(String w, int c) {
        word = w;
        count = c;
    }

    @Override
    public int compareTo(WordCountPair o) {
        return this.getCount() - o.getCount();
    }

    /*@Override
    public int (WordCountPair o1, WordCountPair o2) {
        return o1.getCount() - o1.getCount();
    }*/

    @Override
    public String toString() {
        return this.getWord() + " " + this.getCount();
    }
}